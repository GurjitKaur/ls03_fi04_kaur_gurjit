package view.menue;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.Button;
import java.awt.GridLayout;
import javax.swing.JLabel;
import java.awt.TextField;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Checkbox;
import java.awt.Label;
import java.awt.Panel;

public class CreateAccount extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CreateAccount frame = new CreateAccount();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public CreateAccount() {
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 515);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new GridLayout(0, 3, 0, 0));
		
		Label label_4 = new Label("UserID");
		contentPane.add(label_4);
		
		TextField textField = new TextField();
		contentPane.add(textField);
		
		Panel panel = new Panel();
		contentPane.add(panel);
		
		Label label_5 = new Label("Vorname");
		contentPane.add(label_5);
		
		TextField textField_1 = new TextField();
		contentPane.add(textField_1);
		
		Panel panel_1 = new Panel();
		contentPane.add(panel_1);
		
		Label label_6 = new Label("Nachname");
		contentPane.add(label_6);
		
		TextField textField_2 = new TextField();
		contentPane.add(textField_2);
		
		Panel panel_2 = new Panel();
		contentPane.add(panel_2);
		
		Label label_7 = new Label("Geburtsdatum");
		contentPane.add(label_7);
		
		TextField textField_5 = new TextField();
		contentPane.add(textField_5);
		
		Panel panel_3 = new Panel();
		contentPane.add(panel_3);
		
		Label label_8 = new Label("Stra\u00DFe");
		contentPane.add(label_8);
		
		TextField textField_4 = new TextField();
		contentPane.add(textField_4);
		
		Panel panel_4 = new Panel();
		contentPane.add(panel_4);
		
		Label label_9 = new Label("Hausnummer");
		contentPane.add(label_9);
		
		TextField textField_3 = new TextField();
		contentPane.add(textField_3);
		
		Panel panel_5 = new Panel();
		contentPane.add(panel_5);
		
		Label label_10 = new Label("Postleitzahl");
		contentPane.add(label_10);
		
		TextField textField_6 = new TextField();
		contentPane.add(textField_6);
		
		Panel panel_6 = new Panel();
		contentPane.add(panel_6);
		
		Label label_11 = new Label("Stadt");
		contentPane.add(label_11);
		
		TextField textField_7 = new TextField();
		contentPane.add(textField_7);
		
		Panel panel_7 = new Panel();
		contentPane.add(panel_7);
		
		Label label_12 = new Label("Benutzername");
		contentPane.add(label_12);
		
		TextField textField_10 = new TextField();
		contentPane.add(textField_10);
		
		Panel panel_8 = new Panel();
		contentPane.add(panel_8);
		
		Label label_13 = new Label("Passwort");  //TODO Ausblenden 
		contentPane.add(label_13);
		
		TextField textField_8 = new TextField();
		contentPane.add(textField_8);
		
		Panel panel_9 = new Panel();
		contentPane.add(panel_9);
		
		Label label_14 = new Label("Gehaltsvorstellung");
		contentPane.add(label_14);
		
		TextField textField_9 = new TextField();
		contentPane.add(textField_9);
		
		Panel panel_10 = new Panel();
		contentPane.add(panel_10);
		
		Label label_15 = new Label("Beziehungsstatus");
		contentPane.add(label_15);
		
		TextField textField_11 = new TextField();
		contentPane.add(textField_11);
		
		Panel panel_11 = new Panel();
		contentPane.add(panel_11);
		
		Label label_16 = new Label("Endnote");
		contentPane.add(label_16);
		
		TextField textField_12 = new TextField();
		contentPane.add(textField_12);
		
		Panel panel_12 = new Panel();
		contentPane.add(panel_12);
		
		JLabel label = new JLabel("");
		contentPane.add(label);
		
		JLabel label_1 = new JLabel("");
		contentPane.add(label_1);
		
		JLabel label_2 = new JLabel("");
		contentPane.add(label_2);
		
		Button button_13 = new Button("Abbrechen");
		button_13.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				setVisible(false);
			}
		});
		contentPane.add(button_13);
		
		JLabel label_3 = new JLabel("");
		contentPane.add(label_3);
		
		Button button_14 = new Button("Fertig");
		button_14.addActionListener(new ActionListener() {
			
			//TODO definieren variablen zum speichern
			//TODO 
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		contentPane.add(button_14);
	}

}
